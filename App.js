// @flow
import React, {Component} from 'react';
import {
    AppState,
    Button, FlatList,
    NativeModules,
    ScrollView,
    StyleSheet,
    Text,
    View
} from 'react-native';
import Swipeable from './components/SwipeableComponent';

import Timer from './Timer';

const {RingtoneManager} = NativeModules;

const localRingtones = [
    {label: 'Break End', value: 'break_end', isLocal: true},
    {label: 'Break Start', value: 'break_start', isLocal: true},
    {label: 'Ticking', value: 'ticking', isLocal: true},
    {label: 'Timer End', value: 'timer_end', isLocal: true},
    {label: 'Timer Loop', value: 'timer_loop', isLocal: true},
    {label: 'Timer Start', value: 'timer_start', isLocal: true}
];

type Props = {};
type State = { nativeRingtones: Array<Object>, shortSwipe: boolean, isSwiping: boolean };
export default class App extends Component<Props, State> {

    constructor() {
        super();
        this.state = {
            isSwiping: true,
            nativeRingtones: [],
            shortSwipe: true
        };
    }

    handleAppStateChange = (nextAppState: string) => {
        if (nextAppState.match(/inactive|background/)) {
            RingtoneManager.stop();
        }
    };

    onLongLeftActionComplete = () => {
        console.log('onLongLeftActionComplete');
    };

    onShortLeftActionComplete = () => {
        console.log('onShortLeftActionComplete');
    };

    onLongRightActionComplete = () => {
        console.log('onLongRightActionComplete');
    };

    onShortRightActionComplete = () => {
        console.log('onShortRightActionComplete');
    };

    play = (url: string, isNative: boolean) => {
        RingtoneManager.playRingtone(url, isNative);
    };

    stop = () => {
        RingtoneManager.stop()
    };

    componentDidMount() {
        RingtoneManager.getRingtones(res => {
            this.setState({
                nativeRingtones: res
            })
        });

        AppState.addEventListener('change', this.handleAppStateChange);

        console.log(typeof <View style={[styles.leftSwipeItem, {backgroundColor: 'orange'}]}>
            <Text>Stop</Text>
        </View>);
    }

    componentWillUnmount() {
        AppState.removeEventListener('change', this.handleAppStateChange);
    }

    _keyExtractor = (item) => item.value;

    _renderItem = ({item}) => (
        <Swipeable
            shortLeftContent={
                <View style={[styles.leftSwipeItem, {backgroundColor: 'lightskyblue'}]}>
                    <Text>Start</Text>
                </View>
            }
            longLeftContent={
                <View style={[styles.leftSwipeItem, {backgroundColor: 'orange'}]}>
                    <Text>Stop</Text>
                </View>
            }
            onShortLeftActionComplete={() => {
                this.play(item.value, item.isLocal || false)
            }}
            onLongLeftActionComplete={this.stop}
            shortRightContent={
                <View style={[styles.rightSwipeItem, {backgroundColor: 'lightskyblue'}]}>
                    <Text>Start</Text>
                </View>
            }
            longRightContent={
                <View style={[styles.rightSwipeItem, {backgroundColor: 'orange'}]}>
                    <Text>Stop</Text>
                </View>
            }
            onShortRightActionComplete={() => {
                this.play(item.value, item.isLocal || false)
            }}
            onLongRightActionComplete={this.stop}
            shortActionActivationDistance={100}
            longActionActivationDistance={200}
            onSwipeStart={() => this.setState({isSwiping: true})}
            onSwipeRelease={() => this.setState({isSwiping: false})}>
            <View style={styles.soundItem}>
                <Text>
                    {item.label}
                </Text>
            </View>
        </Swipeable>
    );

    render() {
        return (
            <View style={styles.container}>
                <View style={{height: '50%'}}>
                    <Timer/>
                </View>
                <View style={{height: '50%'}}>
                    <Button title={'STOP'} style={{width: 3, borderRadius: 50}}
                            onPress={this.stop}/>
                    <FlatList
                        data={localRingtones}
                        keyExtractor={this._keyExtractor}
                        renderItem={this._renderItem}
                    />
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        width: '100%',
        paddingTop: 20,
        backgroundColor: '#F5FCFF',
    },
    leftSwipeItem: {
        flex: 1,
        alignItems: 'flex-end',
        justifyContent: 'center',
        paddingRight: 20
    },
    rightSwipeItem: {
        flex: 1,
        alignItems: 'flex-start',
        justifyContent: 'center',
        paddingLeft: 20
    },
    soundItem: {
        height: 50,
        alignItems: 'center',
        justifyContent: 'center',
    }
});
