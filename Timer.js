// @flow
import React, {Component} from 'react';
import {Button, Easing, NativeModules, Picker, Platform, StyleSheet, Text, View} from 'react-native';

import {AnimatedCircularProgress} from 'react-native-circular-progress';

const {RingtoneManager} = NativeModules;
const times = [5, 10, 15];

type Props = {};
type State = {
    time: number,
    timerValue: number,
    isStarted: boolean
}
export default class Timer extends Component<Props, State> {

    circularProgress: AnimatedCircularProgress;
    interval: IntervalID;
    start: number;

    constructor() {
        super();
        this.state = {
            isStarted: false,
            time: times[0] * 60000,
            timerValue: times[0] * 60000
        };
    }

    formatTime = (time: number) => {
        return time > 9 ? time : '0' + time;
    };

    handlePickerValue = (itemValue: number) => {
        this.state.timerValue = itemValue;
        this.setState({
            time: itemValue
        }, () => {
            this.stopTimer();
        });
    };

    handleTimer = () => {
        if (this.state.timerValue - (+new Date() - this.start) > 0) {
            this.setState({
                time: this.state.timerValue - (+new Date() - this.start)
            })
        } else {
            this.stopTimer();
        }
    };

    startTimer = () => {
        this.start = +new Date();
        this.setState({
            isStarted: true
        });
        this.interval = setInterval(this.handleTimer, 1);
        this.circularProgress.animate(100, this.state.timerValue, Easing.linear);
        RingtoneManager.playDefaultRingtone();
    };

    stopTimer = () => {
        this.setState({
            time: this.state.timerValue,
            isStarted: false
        });
        clearInterval(this.interval);
        this.circularProgress.animate(0, 1, Easing.linear);
        RingtoneManager.stop();
    };

    shouldComponentUpdate(_: Object, nextState: Object) {
        return nextState.time - this.state.time < 0 || nextState.time === this.state.timerValue;
    }

    render() {
        const seconds = this.formatTime(Math.trunc(this.state.time / 1000) % 60);
        const minutes = this.formatTime(Math.trunc(this.state.time / 1000 / 60));
        return (
            <View style={styles.container}>
                <Picker
                    selectedValue={this.state.timerValue}
                    style={{ height: 50, width: 120 }}
                    onValueChange={this.handlePickerValue}>
                    {times.map(el => (
                        <Picker.Item key={el} label={`${el} min`} value={el * 60000} />
                    ))}
                </Picker>
                <AnimatedCircularProgress
                    size={200}
                    width={5}
                    fill={0}
                    tintColor="#3d5875"
                    onAnimationComplete={() => console.log('onAnimationComplete')}
                    ref={(ref) => this.circularProgress = ref}
                    backgroundColor="#00e0ff">
                    {
                        (fill) => (
                            <View style={{alignItems: 'center'}}>
                                <Text style={{fontSize: 22, color: 'red'}}>
                                    { minutes + ':' + seconds }
                                </Text>
                                {!this.state.isStarted && <Button title={'Start'} onPress={this.startTimer}/>}
                                {this.state.isStarted && <Button title={'Stop'} onPress={this.stopTimer}/>}
                            </View>
                        )
                    }
                </AnimatedCircularProgress>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    }
});
