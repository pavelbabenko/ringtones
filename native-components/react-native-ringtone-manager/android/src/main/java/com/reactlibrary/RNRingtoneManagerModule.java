
package com.reactlibrary;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.provider.MediaStore;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReadableMap;
import com.facebook.react.bridge.WritableArray;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.bridge.WritableNativeArray;
import com.facebook.react.bridge.WritableNativeMap;

import java.io.File;
import java.io.IOException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RNRingtoneManagerModule extends ReactContextBaseJavaModule {

    private final ReactApplicationContext reactContext;
    private static final String TYPE_ALARM_KEY = "TYPE_ALARM";
    private static final String TYPE_ALL_KEY = "TYPE_ALL";
    private static final String TYPE_NOTIFICATION_KEY = "TYPE_NOTIFICATION";
    private static final String TYPE_RINGTONE_KEY = "TYPE_RINGTONE";

    private MediaPlayer player;

    final static class SettingsKeys {
        public static final String URI = "uri";
        public static final String TITLE = "title";
        public static final String ARTIST = "artist";
        public static final String SIZE = "size";
        public static final String MIME_TYPE = "mimeType";
        public static final String DURATION = "duration";
        public static final String RINGTONE_TYPE = "ringtoneType";
    }

    public RNRingtoneManagerModule(ReactApplicationContext reactContext) {
        super(reactContext);
        this.reactContext = reactContext;
        this.player = new MediaPlayer();
    }

    @Override
    public String getName() {
        return "RingtoneManager";
    }

    @ReactMethod
    public void getRingtones(Callback cb) {
        RingtoneManager manager = new RingtoneManager(reactContext);
        manager.setType(RingtoneManager.TYPE_NOTIFICATION);
        Cursor cursor = manager.getCursor();

        WritableArray list = new WritableNativeArray();

        while (cursor.moveToNext()) {
            String notificationTitle = cursor.getString(RingtoneManager.TITLE_COLUMN_INDEX);
            String notificationUri = manager.getRingtoneUri(cursor.getPosition()).toString();
            WritableMap map = new WritableNativeMap();
            map.putString("label", notificationTitle);
            map.putString("value", notificationUri);
            list.pushMap(map);
        }
        cb.invoke(list);
    }

    @ReactMethod
    public void createRingtone(ReadableMap settings) {
        String uriStr = settings.getString(SettingsKeys.URI);
        File ringtone = new File(uriStr);
        ContentValues values = new ContentValues();
        values.put(MediaStore.MediaColumns.DATA, ringtone.getAbsolutePath());
        values.put(MediaStore.MediaColumns.TITLE, settings.getString(SettingsKeys.TITLE));
        values.put(MediaStore.MediaColumns.SIZE, settings.getInt(SettingsKeys.SIZE));
        values.put(MediaStore.MediaColumns.MIME_TYPE, settings.getString(SettingsKeys.MIME_TYPE));
        values.put(MediaStore.Audio.Media.ARTIST, settings.getString(SettingsKeys.ARTIST));
        values.put(MediaStore.Audio.Media.DURATION, settings.getInt(SettingsKeys.DURATION));
        int ringtoneType = settings.getInt(SettingsKeys.RINGTONE_TYPE);
        values.put(MediaStore.Audio.Media.IS_RINGTONE, isRingtoneType(ringtoneType, RingtoneManager.TYPE_RINGTONE));
        values.put(MediaStore.Audio.Media.IS_NOTIFICATION, isRingtoneType(ringtoneType, RingtoneManager.TYPE_NOTIFICATION));
        values.put(MediaStore.Audio.Media.IS_ALARM, isRingtoneType(ringtoneType, RingtoneManager.TYPE_ALARM));
        values.put(MediaStore.Audio.Media.IS_MUSIC, false);
        if (ringtone.exists() && getCurrentActivity() != null) {
            ContentResolver contentResolver = getCurrentActivity().getContentResolver();
            Uri uri = MediaStore.Audio.Media.getContentUriForPath(ringtone.getAbsolutePath());
            contentResolver.insert(uri, values);
        }
    }

    @ReactMethod
    public void setRingtone(String uri) {

    }

    @ReactMethod
    public void playRingtone(String stringUri, Boolean isLocal) {
        if (this.player.isPlaying()) {
            this.player.reset();
            this.player = new MediaPlayer();
        }
        try {
            Uri uri;
            if (!isLocal) {
                uri = Uri.parse(stringUri);
            } else {
                uri = Uri.parse("android.resource://com.browsers/raw/" + stringUri);
            }
            this.player.setDataSource(reactContext, uri);
            this.player.setAudioStreamType(AudioManager.STREAM_RING);
            this.player.setLooping(true);
            this.player.prepare();
            this.player.start();
        } catch (IllegalArgumentException e) {

            e.printStackTrace();

        } catch (SecurityException e) {

            e.printStackTrace();

        } catch (IllegalStateException e) {

            e.printStackTrace();

        } catch (IOException e) {

            e.printStackTrace();
        }
    }

    @ReactMethod
    public void playDefaultRingtone() {
        Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
        if (this.player.isPlaying()) {
            this.player.reset();
            this.player = new MediaPlayer();
        }
        try {
            this.player.setDataSource(reactContext, uri);
            this.player.setAudioStreamType(AudioManager.STREAM_RING);
            this.player.setLooping(true);
            this.player.prepare();
            this.player.start();
        } catch (IllegalArgumentException e) {

            e.printStackTrace();

        } catch (SecurityException e) {

            e.printStackTrace();

        } catch (IllegalStateException e) {

            e.printStackTrace();

        } catch (IOException e) {

            e.printStackTrace();
        }
    }

    @ReactMethod
    public void stop() {
        this.player.reset();
    }

    @Override
    public Map<String, Object> getConstants() {
        final Map<String, Object> constants = new HashMap<>();
        constants.put(TYPE_ALARM_KEY, RingtoneManager.TYPE_ALARM);
        constants.put(TYPE_ALL_KEY, RingtoneManager.TYPE_ALL);
        constants.put(TYPE_NOTIFICATION_KEY, RingtoneManager.TYPE_NOTIFICATION);
        constants.put(TYPE_RINGTONE_KEY, RingtoneManager.TYPE_RINGTONE);
        return constants;
    }

    /**
     * Returns true when the given ringtone type matches the ringtone to compare.
     * Will default to true if the given ringtone type is RingtoneManager.TYPE_ALL.
     * @param ringtoneType ringtone type given
     * @param ringtoneTypeToCompare ringtone type to compare to
     * @return true if the type matches or is TYPE_ALL
     */
    private boolean isRingtoneType(int ringtoneType, int ringtoneTypeToCompare) {
        return ringtoneTypeToCompare == ringtoneType || RingtoneManager.TYPE_ALL == ringtoneType;
    }
}