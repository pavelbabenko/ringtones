// @flow
import React, {PureComponent} from 'react';
import Swipeable from 'react-native-swipeable';

type Props = {
    children: Object,
    shortActionActivationDistance: number,
    longActionActivationDistance: number,
    shortLeftContent: Object,
    longLeftContent: Object,
    shortRightContent: Object,
    longRightContent: Object,
    onShortLeftActionComplete: Function,
    onLongLeftActionComplete: Function,
    onShortRightActionComplete: Function,
    onLongRightActionComplete: Function,
    onSwipeStart: Function,
    onSwipeRelease: Function
};
type State = { shortSwipe: boolean };
export default class SwipeableComponent extends PureComponent<Props, State> {

    constructor(props: Props) {
        super(props);
        this.state = {
            shortSwipe: true
        };
    }

    onLeftActionComplete = () => {
        const {onShortLeftActionComplete, onLongLeftActionComplete} = this.props;
        if (this.state.shortSwipe) {
            onShortLeftActionComplete();
        } else {
            onLongLeftActionComplete();
        }
        if (this.state.shortSwipe === false) {
            this.setState({shortSwipe: true});
        }
    };

    onRightActionComplete = () => {
        const {onShortRightActionComplete, onLongRightActionComplete} = this.props;
        if (this.state.shortSwipe) {
            onShortRightActionComplete();
        } else {
            onLongRightActionComplete();
        }
        if (this.state.shortSwipe === false) {
            this.setState({shortSwipe: true});
        }
    };

    onSwipe = (_: Object, ges: Object) => {
        const {longActionActivationDistance} = this.props;
        if (this.state.shortSwipe === true && Math.abs(ges.moveX - ges.x0) >= longActionActivationDistance) {
            this.setState({
                shortSwipe: false
            })
        } else if (this.state.shortSwipe === false && Math.abs(ges.moveX - ges.x0) < longActionActivationDistance) {
            this.setState({
                shortSwipe: true
            })
        }
    };

    render() {
        const {shortSwipe} = this.state;
        const {
            shortActionActivationDistance,
            longActionActivationDistance,
            shortLeftContent,
            longLeftContent,
            shortRightContent,
            longRightContent,
            onSwipeStart,
            onSwipeRelease
        } = this.props;
        return (
            <Swipeable
                leftContent={shortSwipe ? shortLeftContent : longLeftContent}
                onLeftActionComplete={this.onLeftActionComplete}
                leftActionActivationDistance={
                    shortSwipe ? shortActionActivationDistance : longActionActivationDistance
                }
                rightContent={shortSwipe ? shortRightContent : longRightContent}
                onRightActionComplete={this.onRightActionComplete}
                rightActionActivationDistance={
                    shortSwipe ? shortActionActivationDistance : longActionActivationDistance
                }
                onSwipeMove={this.onSwipe}
                onSwipeStart={onSwipeStart}
                onSwipeRelease={onSwipeRelease}
            >
                {this.props.children}
            </Swipeable>
        );
    }
}
